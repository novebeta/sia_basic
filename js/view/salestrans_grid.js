jun.SalestransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales Transaction",
    id: 'docs-jun.SalestransGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref'
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer'
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer'
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl'
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Store',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBankLib.getTotalCount() === 0) {
            jun.rztBankLib.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztCardLib.getTotalCount() === 0) {
            jun.rztCardLib.load();
        }
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztPaketCmp.getTotalCount() === 0) {
            jun.rztPaketCmp.load();
        }
        if (jun.rztPaketLib.getTotalCount() === 0) {
            jun.rztPaketLib.load();
        }
        jun.rztSalestrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglsalesgridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztSalestrans;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Sales',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Sales',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Sales',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'RETURN SALES ALL ITEM',
                    ref: '../btnRetur'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglsalesgridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.SalestransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        this.btnRetur.on('Click', this.returAllRec, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    returAllRec: function () {
        Ext.MessageBox.confirm('Question', 'Are you sure want return all item this sales?', this.returAll, this);
    },
    returAll: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/returall',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                //qz.appendHTML('<html><pre>'+response.msg+'</pre></html>');
                //qz.printHTML();
                //qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
                //qz.printHTML();
                opencashdrawer();
                printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
                printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printSales: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/print',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
//                qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                while (!qz.isDoneAppending()) {}
//                qz.append("\x1B\x40"); // 1
//                //qz.append("\x1B\x21\x08"); // 2
//                qz.append("\x1B\x21\x01"); // 3
//                qz.append(response.msg);
//                qz.append("\x1D\x56\x41"); // 4
//                qz.append("\x1B\x40"); // 5
//                qz.print();
//                qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
//                qz.printHTML();
                printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztCustomersSalesCmp.baseParams = {
            customer_id: this.record.data.customer_id
        };
        jun.rztCustomersSalesCmp.load();
        jun.rztCustomersSalesCmp.baseParams = {};
    },
    //editForm: function () {
    //    var selectedz = this.sm.getSelected();
    //    if (selectedz == undefined) {
    //        Ext.MessageBox.alert("Warning", "You have not selected a transaction");
    //        return;
    //    }
    //    var idz = selectedz.json.salestrans_id;
    //    var form = new jun.SalestransWin({modez: 1, id: idz});
    //    form.show(this);
    //    form.formz.getForm().loadRecord(this.record);
    //    //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
    //    //jun.rztCustomersCmp.un('load', this.editForm, this);
    //    jun.rztSalestransDetails.baseParams = {
    //        salestrans_id: idz
    //    };
    //    jun.rztSalestransDetails.load();
    //    jun.rztSalestransDetails.baseParams = {};
    //    jun.rztPayment.baseParams = {
    //        salestrans_id: idz
    //    };
    //    jun.rztPayment.load();
    //    jun.rztPayment.baseParams = {};
    //},
    loadForm: function () {
        //jun.rztCustomersCmp.un('load', this.editForm, this);
        var form = new jun.SalestransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.SalestransWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        //jun.rztCustomersCmp.un('load', this.editForm, this);
        jun.rztSalestransDetails.baseParams = {
            salestrans_id: idz
        };
        jun.rztSalestransDetails.load();
        jun.rztSalestransDetails.baseParams = {};
        jun.rztPayment.baseParams = {
            salestrans_id: idz
        };
        jun.rztPayment.load();
        jun.rztPayment.baseParams = {};
        jun.rztPaketTrans.baseParams = {
            salestrans_id: idz
        };
        jun.rztPaketTrans.load();
        jun.rztPaketTrans.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/delete/id/' + record.json.salestrans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSalestrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.beautytransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Service",
    id: 'docs-jun.beautytransGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            width: 200
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 200
        },
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 200
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 200
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Beauty 1',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty_id',
            renderer: jun.renderBeauty
        },
        {
            header: 'Beauty 2',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty2_id',
            renderer: jun.renderBeauty
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztBeutytrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglbeutygridid').getValue()
                    }
                }
            }
        });
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        this.store = jun.rztBeutytrans;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Edit Service',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Post / Final',
                    ref: '../btnFinal'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglbeutygridid'
                }
            ]
        };
        jun.beautytransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnFinal.on('Click', this.onbtnFinalclick, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    onbtnFinalclick: function () {
        var tgl = this.tgl.getValue();
        if (tgl == '' || tgl == undefined) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Date must selected!',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }
        Ext.MessageBox.confirm('Confirmation',
            'Are you really sure POST / FINAL data service on ' + tgl.format('F j, Y') + '?', function (btn) {
                if (btn == 'no') {
                    return;
                }
                Ext.Ajax.request({
                    url: 'SalestransDetails/final/',
                    method: 'POST',
                    scope: this,
                    params: {
                        tgl: tgl
                    },
                    success: function (f, a) {
                        jun.rztBeutytrans.reload();
                        var response = Ext.decode(f.responseText);
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: response.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }, this);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.beautytransWin({modez: 1, idrecord: idz, final: selectedz.json.final});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
    }
});
jun.HistoryGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "History",
    id: 'docs-jun.SalestransGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref'
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer'
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer'
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl'
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBankTransCmp.getTotalCount() === 0) {
            jun.rztBankTransCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztHistory.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglhistorygridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztHistory;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add History',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View History',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglhistorygridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.HistoryGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    editForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.HistoryWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztCustomersCmp.un('load', this.editForm, this);
    },
    loadForm: function () {
        jun.rztCustomersCmp.un('load', this.editForm, this);
        var form = new jun.HistoryWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        jun.rztCustomersCmp.on('load', this.editForm, this);
        jun.rztCustomersCmp.baseParams = {
            customer_id: selectedz.json.customer_id
        };
        jun.rztCustomersCmp.reload();
        jun.rztCustomersCmp.baseParams = {};
        jun.rztSalestransDetails.baseParams = {
            salestrans_id: idz
        };
        jun.rztSalestransDetails.load();
        jun.rztSalestransDetails.baseParams = {};
    }
});