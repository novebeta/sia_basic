jun.PaketDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PaketDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaketDetailsStoreId',
            url: 'PaketDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'paket_details_id'},
                {name: 'kategori_name'},
                {name: 'paket_id'},
                {name: 'price', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var total = this.sum("price");
        Ext.getCmp('regular_priceid').setValue(total);
        var proposed = parseFloat(Ext.getCmp('proposed_priceid').getValue());
        var total_disc = total - proposed;
        Ext.getCmp('total_discid').setValue(total_disc);
        //var disc = round((total_disc / total) * 100,10);
        //Ext.getCmp('discid').setValue(disc);
    }
});
jun.rztPaketDetails = new jun.PaketDetailsstore();
jun.PaketDetailsTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PaketDetailsTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaketDetailsTransStoreId',
            url: 'PaketDetails/Trans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'paket_details_id'},
                {name: 'kategori_name'},
                {name: 'paket_trans_id'},
                {name: 'barang_id'},
                {name: 'disc'},
                {name: 'ketpot'}
            ]
        }, cfg));
    }
});
jun.rztTransPaketDetails = new jun.PaketDetailsTransstore();
//jun.rztPaketDetails.load();
