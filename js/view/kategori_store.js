jun.Kategoristore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Kategoristore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KategoriStoreId',
            url: 'Kategori',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kategori_id'},
                {name: 'nama_kategori'},
                {name: 'have_stock'}
            ]
        }, cfg));
    }
});
jun.rztKategori = new jun.Kategoristore();
//jun.rztKategori.load();
