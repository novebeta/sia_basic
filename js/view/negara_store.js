jun.Negarastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Negarastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'NegaraStoreId',
            url: 'Negara',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'negara_id'},
                {name: 'nama_negara'}
            ]
        }, cfg));
    }
});
jun.rztNegara = new jun.Negarastore();
jun.rztNegaraLib = new jun.Negarastore();
jun.rztNegaraCmp = new jun.Negarastore();
//jun.rztNegara.load();
