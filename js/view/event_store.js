jun.Eventstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Eventstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EventStoreId',
            url: 'Event',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'event_id'},
                {name: 'event_name'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztEvent = new jun.Eventstore();
jun.rztEventCmp = new jun.Eventstore();
jun.rztEventLib = new jun.Eventstore();
//jun.rztEvent.load();
