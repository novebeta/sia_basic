jun.PaketTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PaketTrans",
    arus: 1,
    id: 'docs-jun.PaketTransGrid',
    //iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Packages Name',
            sortable: true,
            resizable: true,
            dataIndex: 'paket_id',
            width: 100,
            renderer: jun.renderPaket
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztPaketTrans;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Package',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete Package',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.PaketTransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        //this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PaketTransWin({modez: 0, arus: this.arus});
        form.show();
    },
    //loadEditForm: function () {
    //    var selectedz = this.sm.getSelected();
    //    //var dodol = this.store.getAt(0);
    //    if (selectedz == undefined) {
    //        Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
    //        return;
    //    }
    //    var idz = selectedz.json.paket_trans_id;
    //    var form = new jun.PaketTransWin({modez: 1, id: idz});
    //    form.show(this);
    //    form.formz.getForm().loadRecord(this.record);
    //},
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        var paket_trans_id = record.data.paket_trans_id;
        var detail_trans;
        if (this.arus == 1) {
            detail_trans = jun.rztSalestransDetails;
        }else{
            detail_trans = jun.rztReturSalestransDetails;
        }
        detail_trans.each(function (item, index, totalItems) {
            if (item.data.paket_trans_id == paket_trans_id) {
                detail_trans.remove(item);
                var barang = jun.getBarang(item.data.barang_id);
                menuDetil(barang.data.kode_barang, "-" + record.data.total, 'TOTAL',
                    parseFloat(Ext.getCmp("totalid").getValue()));
            }
        });
        this.store.remove(record);
    }
});
