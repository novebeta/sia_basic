jun.Jualstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Jualstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JualStoreId',
            url: 'Jual',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jual_id'},
                {name: 'price'},
                {name: 'cost'},
                {name: 'store'},
                {name: 'up'},
                {name: 'barang_id'},

            ]
        }, cfg));
    }
});
jun.rztJual = new jun.Jualstore();
jun.rztJualLib = new jun.Jualstore();
jun.rztJualCmp = new jun.Jualstore();
//jun.rztJual.load();
