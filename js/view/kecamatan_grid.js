jun.KecamatanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sub District",
    id: 'docs-jun.KecamatanGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Sub District Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kecamatan',
            width: 100
        },
        {
            header: 'City Name',
            sortable: true,
            resizable: true,
            dataIndex: 'kota_id',
            renderer: jun.renderKota,
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztKotaLib.on({
            scope: this,
            load: {
                fn: function (a, b, c) {
                    if (jun.rztKotaCmp.getTotalCount() === 0) {
                        jun.rztKotaCmp.add(b);
                    }
                    this.store.baseParams = {mode: "grid"};
                    this.store.reload();
                    this.store.baseParams = {};
                }
            }
        });
        if (jun.rztKotaLib.getTotalCount() === 0) {
            jun.rztKotaLib.load();
        }
        this.store = jun.rztKecamatan;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Sub District',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Sub District',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.KecamatanGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KecamatanWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a sub district");
            return;
        }
        var idz = selectedz.json.kecamatan_id;
        var form = new jun.KecamatanWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a sub district");
            return;
        }
        Ext.Ajax.request({
            url: 'Kecamatan/delete/id/' + record.json.kecamatan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKecamatan.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
