jun.BankTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BankTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BankTransStoreId',
            url: 'BankTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'trans_no'},
                {name: 'ref'},
                {name: 'trans_date'},
                {name: 'amount'},
                {name: 'bank_act_asal'},
                {name: 'bank_act_tujuan'},
                {name: 'charge'},
                {name: 'memo'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztBankTrans = new jun.BankTransstore();
//jun.rztBankTrans.load();
