jun.Msdstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Msdstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MsdStoreId',
            url: 'Msd',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'msd_id'},
                {name: 'customer_id'},
                {name: 'salestrans_id'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'up'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'store'},
                {name: 'nama_customer'},
                {name: 'doc_ref_sales'}
            ]
        }, cfg));
    }
});
jun.rztMsd = new jun.Msdstore();
//jun.rztMsd.load();
