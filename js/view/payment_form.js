jun.PaymentWin = Ext.extend(Ext.Window, {
    title: 'Payment',
    modez: 1,
    width: 400,
    height: 200,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-Payment',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmp,
                        ref: '../bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Card Number',
                        hideLabel: false,
                        //hidden:true,
                        name: 'card_number',
                        id: 'card_numberid',
                        ref: '../card_number',
                        enableKeyEvents: true,
                        maxLength: 16,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Card Type',
                        store: jun.rztCardCmp,
                        ref: '../card',
                        hiddenName: 'card_id',
                        valueField: 'card_id',
                        displayField: 'card_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Amount',
                        hideLabel: false,
                        //hidden:true,
                        name: 'amount',
                        id: 'amountid',
                        ref: '../amount',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PaymentWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.bank.on('select', this.onBankChange, this);
        this.card_number.on("specialkey", this.onCardnumber, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    onActivate: function(){
        this.btnDisabled(false);
    },
    onCardnumber: function (f, e) {
        if (e.getKey() == e.ENTER) {
            var val = this.card_number.getValue();
            if (val == "" || val == undefined) {
                return;
            }
            var p = new SwipeParserObj(val);
            this.card_number.setValue(p.account);
            Ext.getCmp('doc_refid').focus(false, 100);
        }
    },
    onBankChange: function () {
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        if (bank == SYSTEM_BANK_CASH) {
            this.card_number.reset();
            this.card.reset();
            this.card_number.setDisabled(true);
            this.card.setDisabled(true);
        } else {
            this.card_number.setDisabled(false);
            this.card.setDisabled(false);
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        var card_number = this.card_number.getValue();
        var card_id = this.card.getValue();
        var amount = this.amount.getValue();

        var c = jun.rztPayment.recordType,
            d = new c({
                bank_id: bank,
                amount: amount,
                card_number: card_number,
                card_id: card_id
            });
        jun.rztPayment.add(d);
        this.btnDisabled(false);
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});