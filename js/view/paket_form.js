jun.PaketWin = Ext.extend(Ext.Window, {
    title: 'Package',
    modez: 1,
    width: 615,
    height: 505,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-Paket',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Package Name:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Package Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'paket_name',
                        id: 'paket_nameid',
                        ref: '../paket_name',
                        maxLength: 100,
                        x: 95,
                        y: 2,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Proposed Price:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'proposed_price',
                        hideLabel: false,
                        //hidden:true,
                        name: 'proposed_price',
                        id: 'proposed_priceid',
                        ref: '../proposed_price',
                        maxLength: 30,
                        width: 175,
                        value: 0,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Regular Price:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'regular_price',
                        id: 'regular_priceid',
                        ref: '../regular_price',
                        width: 175,
                        value: 0,
                        readOnly: true,
                        x: 95,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Total Disc:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_disc',
                        id: 'total_discid',
                        ref: '../total_disc',
                        width: 175,
                        value: 0,
                        readOnly: true,
                        x: 400,
                        y: 32
                    },
                    //{
                    //    xtype: "label",
                    //    text: "Disc (%):",
                    //    x: 295,
                    //    y: 35
                    //},
                    //{
                    //    xtype: 'numericfield',
                    //    fieldLabel: 'disc',
                    //    hideLabel: false,
                    //    //hidden:true,
                    //    name: 'disc',
                    //    id: 'discid',
                    //    ref: '../disc',
                    //    width: 175,
                    //    value: 0,
                    //    x: 400,
                    //    y: 32
                    //},
                    new jun.PaketDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 190,
                        frameHeader: !1,
                        header: !1
                    }),
                    new jun.PaketBarangGrid({
                        x: 5,
                        y: 260,
                        height: 160,
                        frameHeader: !1,
                        ref: '../paket_barang',
                        header: !1
                    }),
                    {
                        xtype: 'hidden',
                        id: 'paket_details_idid'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PaketWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.proposed_price.on('change', this.onproposedChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.paket_barang.setVisible(true);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            this.paket_barang.setVisible(false);
        }
    },
    onWinClose: function () {
        jun.rztPaketDetails.removeAll();
        jun.rztPaketBarang.removeAll();
    },
    onproposedChange: function () {
        jun.rztPaketDetails.refreshData();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Paket/update/id/' + this.id;
        } else {
            urlz = 'Paket/create/';
        }
        Ext.getCmp('form-Paket').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPaketDetails.data.items, "data")),
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPaket.reload();
                jun.rztPaketLib.reload();
                jun.rztPaketCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Paket').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});