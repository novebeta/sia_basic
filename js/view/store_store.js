jun.Storestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Storestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StoreStoreId',
            url: 'Store',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'store_kode'},
                {name: 'nama_store'},
                {name: 'wilayah_id'}
            ]
        }, cfg));
    }
});
jun.rztStore = new jun.Storestore();
jun.rztStoreLib = new jun.Storestore();
jun.rztStoreCmp = new jun.Storestore();
//jun.rztStore.load();
