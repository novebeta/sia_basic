<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <link rel="shortcut icon" href="<?php echo bu();?>/images/icon-natasha.gif" />
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
        <style>
            #drop {
                border: 2px dashed #BBBBBB;
                border-radius: 5px;
                color: #BBBBBB;
                font: 20pt bold, "Vollkorn";
                padding: 25px;
                text-align: center;
            }
            * {
                font-size: 12px;
                font-family: Candara;
            }
        </style>
    </head>
    <body>
        <script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
        <script>
            DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
            var BASE_URL = '<?=bu()===""?"/":bu();?>';
            // Load native UI library.
//        var gui = require('nw.gui');
        </script>        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>                  
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/login.js"></script>        
        <?php echo $content; ?>
        <script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
    </body>
</html>
