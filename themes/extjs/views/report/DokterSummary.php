<h1>Doctors Service Summary</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Doctors Service Summary';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Doctor Name',
            'name' => 'nama_dokter'
        ),
        array(
            'header' => 'Service Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Service Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Total Services Tips',
            'name' => 'tip',
            'value' => function ($data) {
                return format_number_report($data['tip'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    ),
));
?>