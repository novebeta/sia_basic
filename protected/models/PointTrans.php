<?php

Yii::import('application.models._base.BasePointTrans');
class PointTrans extends BasePointTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->point_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->point_trans_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function save_point_trans($type_no, $trans_no, $ref, $tgl, $qty, $customer_id, $event_id, $store)
    {
        $point = new PointTrans;
        $point->type_no = $type_no;
        $point->trans_no = $trans_no;
        $point->ref = $ref;
        $point->tgl = $tgl;
        $point->qty = $qty;
        $point->customer_id = $customer_id;
        $point->event_id = $event_id;
        $point->store = $store;
        if (!$point->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Point Trans')) . CHtml::errorSummary($point));
        }
    }
    public static function total_point($customer_id, $event_id)
    {
        $command = Yii::app()->db->createCommand("select COALESCE(sum(qty),0) qty from nscc_point_trans npt
          where npt.customer_id = :customer_id AND npt.event_id = :event_id;");
        return $command->queryScalar(array(":customer_id" => $customer_id, ':event_id' => $event_id));
    }
}