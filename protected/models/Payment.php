<?php
Yii::import('application.models._base.BasePayment');
class Payment extends BasePayment
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function add_payment($bank, $salestrans_id, $card_number, $amount, $card_id = null, $kembali = 0, $ulpt = 0)
    {
        $pay = new Payment;
        $pay->bank_id = $bank;
        $pay->salestrans_id = $salestrans_id;
        $pay->card_number = $card_number;
        $pay->amount = $amount;
        $pay->kembali = $kembali;
        $pay->ulpt = $ulpt;
        $pay->card_id = $card_id;
        if (!$pay->save()) {
            throw new Exception(t('save.fail', 'app') . CHtml::errorSummary($pay));
        }
    }
    public function beforeValidate()
    {
        if ($this->payment_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->payment_id = $uuid;
        }
        return parent::beforeValidate();
    }
}