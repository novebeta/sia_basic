<?php
Yii::import('application.models._base.BasePaketBarang');

class PaketBarang extends BasePaketBarang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
}