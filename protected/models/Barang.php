<?php

Yii::import('application.models._base.BaseBarang');
class Barang extends BaseBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function count_biaya_beli($unit, $harga, $store)
    {
        $jual = Jual::model()->findByAttributes(array('barang_id' => $this->barang_id, 'store' => $store));
        $jml_stok = StockMoves::get_saldo_item($this->barang_id, $store);
        $harga_lama = $jual->cost;
        $total_lama = round($jml_stok * $harga_lama, 2);
        $total_baru = $harga;
        $jml_baru = $jml_stok + $unit;
        $harga_baru = round(($total_baru + $total_lama) / $jml_baru, 2);
        $jual->cost = $harga_baru;
        if (!$jual->save()) {
            throw new Exception(t('save.fail', 'app') . CHtml::errorSummary($jual));
        }
    }
    public function get_coa_sales($store){
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return null;
        }else{
            return $grupattr->coa_jual;
        }
    }
    public function get_coa_sales_hpp($store){
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return null;
        }else{
            return $grupattr->coa_sales_hpp;
        }
    }
    public function get_coa_sales_disc($store){
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return null;
        }else{
            return $grupattr->coa_sales_disc;
        }
    }
    public function get_tax($store)
    {
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return 0;
        } else {
            if ($grupattr->tax == 1 && $grupattr->vat > 0) {
                return round($grupattr->vat / 100, 2);
            } else {
                return 0;
            }
        }
    }
    public function get_cost($store)
    {
        $jual = Jual::model()->findByAttributes(array('barang_id' => $this->barang_id, 'store' => $store));
        if ($jual == null) {
            return 0;
        } else {
            return $jual->cost;
        }
    }
    public function get_price($store)
    {
        $jual = Jual::model()->findByAttributes(array('barang_id' => $this->barang_id, 'store' => $store));
        if ($jual == null) {
            return 0;
        } else {
            return $jual->price;
        }
    }
    public function beforeValidate()
    {
        if ($this->barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
}