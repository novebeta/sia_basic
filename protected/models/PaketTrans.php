<?php
Yii::import('application.models._base.BasePaketTrans');

class PaketTrans extends BasePaketTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_trans_id = $uuid;
        }
        return parent::beforeValidate();
    }
}