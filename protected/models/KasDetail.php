<?php
Yii::import('application.models._base.BaseKasDetail');
class KasDetail extends BaseKasDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->kas_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function delete__($kas_id)
    {
        KasDetail::model()->updateAll(array('visible' => 0), 'kas_id = :kas_id', array(':kas_id' => $kas_id));
    }
}