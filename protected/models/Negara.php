<?php

Yii::import('application.models._base.BaseNegara');

class Negara extends BaseNegara
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->negara_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->negara_id = $uuid;
        }
        return parent::beforeValidate();
    }
}