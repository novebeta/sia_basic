<?php

Yii::import('application.models._base.BaseBankTrans');

class BankTrans extends BaseBankTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->bank_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bank_trans_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }

    public static function get_balance($id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(nbt.amount),0) AS total
        FROM nscc_bank_trans nbt
        WHERE nbt.visible = 1 AND nbt.bank_id = :bank_id");
        return $comm->queryScalar(array(':bank_id' => $id));
    }
    public static function get_list_bank_transfer($tgl, $limit, $offset)
    {
        $comm = Yii::app()->db->createCommand("SELECT a.*,b.bank_act_tujuan,COALESCE(c.charge,0) charge,d.memo_ memo FROM
        (SELECT nbt1.type_,nbt1.trans_no,nbt1.ref,nbt1.tgl trans_date,abs(nbt1.amount) amount,nbt1.bank_id bank_act_asal,nbt1.store
        FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount < 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) a 
        INNER JOIN (SELECT nbt1.trans_no,nbt1.bank_id bank_act_tujuan FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount >= 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) b ON a.trans_no = b.trans_no
        LEFT JOIN (SELECT nbt1.trans_no,ABS(nbt1.amount) charge FROM nscc_bank_trans AS nbt1
        WHERE nbt1.type_ = 12 AND nbt1.tgl = :tgl AND nbt1.visible = 1) c ON b.trans_no = c.trans_no 
        LEFT JOIN (SELECT DISTINCT(nc.type_no),nc.memo_ FROM nscc_comments nc WHERE nc.type = 11 AND nc.date_ = :tgl) d 
        ON a.trans_no = d.type_no LIMIT $offset,$limit");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function count_get_list_bank_transfer($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT count(*) FROM
        (SELECT nbt1.type_,nbt1.trans_no,nbt1.ref,nbt1.tgl trans_date,abs(nbt1.amount) amount,nbt1.bank_id bank_act_asal,nbt1.store
        FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount < 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) a 
        INNER JOIN (SELECT nbt1.trans_no,nbt1.bank_id bank_act_tujuan FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount >= 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) b ON a.trans_no = b.trans_no
        LEFT JOIN (SELECT nbt1.trans_no,ABS(nbt1.amount) charge FROM nscc_bank_trans AS nbt1
        WHERE nbt1.type_ = 12 AND nbt1.tgl = :tgl AND nbt1.visible = 1) c ON b.trans_no = c.trans_no 
        LEFT JOIN (SELECT DISTINCT(nc.type_no),nc.memo_ FROM nscc_comments nc WHERE nc.type = 11 AND nc.date_ = :tgl) d 
        ON a.trans_no = d.type_no");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }

    public static function get_list_bank_transfer_by_trans_no($trans_no)
    {
        $comm = Yii::app()->db->createCommand("SELECT nbt.trans_no,nbt.ref,nbt.tgl trans_date,nbt.amount,
        (SELECT nbt1.bank_id FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount < 0 AND nbt1.trans_no = nbt.trans_no) bank_act_asal,
        (SELECT nbt1.bank_id FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount > 0 AND nbt1.trans_no = nbt.trans_no) bank_act_tujuan,
        IFNULL((SELECT ABS(nbt1.amount) FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 12 AND nbt1.trans_no = nbt.trans_no LIMIT 1),0) charge,
        nc.memo_ memo
        FROM nscc_bank_trans AS nbt
        LEFT JOIN nscc_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
        WHERE nbt.type_ = 11 AND nbt.trans_no = :trans_no
        GROUP BY trans_no");
        $comm->setFetchMode(PDO::FETCH_OBJ);
        return $comm->queryRow(true,array(':trans_no' => $trans_no));
    }
}