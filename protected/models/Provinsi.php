<?php

Yii::import('application.models._base.BaseProvinsi');

class Provinsi extends BaseProvinsi
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->provinsi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->provinsi_id = $uuid;
        }
        return parent::beforeValidate();
    }
}