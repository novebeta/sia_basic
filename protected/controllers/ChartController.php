<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 07/01/2015
 * Time: 16:20
 */
class ChartController extends GxController
{
    public function actionCustAtt()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
//        $_POST['from'] = '2014-10-23';
//        $_POST['to'] = '2014-12-31';
//        $_POST['store'] = 'SUB01';
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
SELECT @row_number:=@row_number+1 AS x, b.* FROM (SELECT DATE_FORMAT(a.Date,'%d %b %y') label,COUNT(DISTINCT(ns.customer_id)) y
from (
    select CURDATE() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
    from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
) a LEFT JOIN nscc_salestrans AS ns ON a.Date = ns.tgl $where
WHERE a.Date >= :from AND a.Date <= :to GROUP BY a.Date
ORDER BY a.Date) b,(SELECT @row_number:=0) AS t");
//            $comm->setFetchMode(PDO::FETCH_OBJ);
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('custatt', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from']) . " - " . sql2date($_POST['to'])
            ));
        }
    }
    public function actionNewCust()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
//        $_POST['from'] = '2014-10-23';
//        $_POST['to'] = '2014-12-31';
//        $_POST['store'] = 'SUB01';
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND nc.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
SELECT @row_number:=@row_number+1 AS x, b.* FROM (select DATE_FORMAT(a.Date,'%d %b %y') label,COUNT(nc.awal) y
from (
    select CURDATE() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
    from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
) a LEFT JOIN nscc_customers AS nc ON a.Date = DATE(nc.awal) $where
WHERE a.Date >= :from AND a.Date <= :to
GROUP BY a.Date
ORDER BY a.Date) b,(SELECT @row_number:=0) AS t");
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('newCust', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from']) . " - " . sql2date($_POST['to'])
            ));
        }
    }
    public function actionSalesGrup()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT ng.nama_grup indexLabel,ng.nama_grup legendText,Sum(nsd.total) AS y
        FROM nscc_grup AS ng
        INNER JOIN nscc_barang AS nb ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        WHERE ns.tgl >= :from AND ns.tgl <= :to $where
        GROUP BY ng.nama_grup");
//            $comm->setFetchMode(PDO::FETCH_OBJ);
            $array = $comm->queryAll(true, $param);
            $total = array_sum(array_column($array, 'y'));
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('salesGrup', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from']) . " - " . sql2date($_POST['to']),
                'total' => $total
            ));
        }
    }
    public function actionTopCust()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $limit = $_POST['limit'];
            $param = array(
                ':from' => $_POST['from'],
                ':to' => $_POST['to']
            );
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT nc.nama_customer label,Sum(ns.total) y
            FROM nscc_salestrans AS ns
            INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
            WHERE ns.tgl >= :from AND ns.tgl <= :to $where
            GROUP BY nc.nama_customer
            ORDER BY SUM(ns.total) DESC
            LIMIT $limit");
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('topcust', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from'], 'dd MMM yy') . " - " . sql2date($_POST['to'], 'dd MMM yy'),
                'limit' => $limit
            ));
        }
    }
    public function actionTopSalesGrup()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = $grup = "";
            $limit = $_POST['limit'];
            $param = array(
                ':from' => $_POST['from'],
                ':to' => $_POST['to']
            );
            if ($_POST['grup_id'] != null) {
                $grup = "AND nb.grup_id = :grup_id";
                $param[':grup_id'] = $_POST['grup_id'];
            }
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT nb.kode_barang label,SUM(nsd.qty) y
            FROM nscc_salestrans AS ns
            INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
            INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
            WHERE ns.tgl >= :from AND ns.tgl <= :to $grup $where
            GROUP BY nb.kode_barang
            ORDER BY SUM(nsd.qty) DESC
            LIMIT $limit");
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            if ($_POST['grup_id'] != null) {
                $grup = Grup::model()->findByPk($_POST['grup_id']);
                $grup_name = $grup->nama_grup;
            } else {
                $grup_name = "All Group";
            }
            $this->render('topsalesgrup', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from'], 'dd MMM yy') . " - " . sql2date($_POST['to'], 'dd MMM yy'),
                'limit' => $limit,
                'grup_name' => $grup_name
            ));
        }
    }
}