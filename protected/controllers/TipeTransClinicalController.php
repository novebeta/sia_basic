<?php
class TipeTransClinicalController extends GxController
{
    public function actionIn()
    {
        $criteria = new CDbCriteria();
        $model = ClinicalIn::model()->findAll($criteria);
        $total = ClinicalIn::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionOut()
    {
        $criteria = new CDbCriteria();
        $model = ClinicalOut::model()->findAll($criteria);
        $total = ClinicalOut::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}