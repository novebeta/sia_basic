<?php
class PaketController extends GxController
{
    public function actionCreate()
    {
        $model = new Paket;
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Paket'][$k] = $v;
                }
                $_POST['Paket']['store'] = STOREID;
                $model->attributes = $_POST['Paket'];
                $msg = "Data gagal disimpan.";
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Paket')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $paket_detail = new PaketDetails;
                    $_POST['PaketDetails']['kategori_name'] = $detil['kategori_name'];
                    $_POST['PaketDetails']['price'] = get_number($detil['price']);
                    $_POST['PaketDetails']['paket_id'] = $model->paket_id;
                    $paket_detail->attributes = $_POST['PaketDetails'];
                    if (!$paket_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Paket Detail')) . CHtml::errorSummary($paket_detail));
                    }
                }
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Paket');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Paket'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Paket'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->paket_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->paket_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Paket')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Paket::model()->findAll($criteria);
        $total = Paket::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}