<?php
class DiskonController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Diskon'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $result = Diskon::save_diskon($_POST['Diskon']['barang_id'], $_POST['Diskon']['status_cust_id'], $_POST['Diskon']['value']);
            if ($result > 0) {
                $status = true;
                $msg = t('save.success','app'); // . $model->price_id;
            } else {
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionCreateByGrup()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Diskon'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $result = Diskon::save_diskon_by_grup($_POST['Diskon']['status_cust_id'], $_POST['Diskon']['grup_id'], $_POST['Diskon']['value']);
            if ($result > 0) {
                $status = true;
                $msg = t('save.success','app'); // . $model->price_id;
            } else {
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Diskon');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Diskon'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Diskon'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->diskon_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->diskon_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Diskon::model()->findAll($criteria);
        $total = Diskon::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}