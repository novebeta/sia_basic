<?php
class TransferItemDetailsController extends GxController
{
    public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("transfer_item_id = :transfer_item_id");
        $criteria->params = array(':transfer_item_id'=>$_POST['transfer_item_id']);
        $model = TransferItemDetails::model()->findAll($criteria);
        $total = TransferItemDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->select = "transfer_item_details_id,-qty qty,barang_id,transfer_item_id,
        price,-total total,disc,-discrp discrp,-bruto bruto,vat,-vatrp vatrp,disc1,-discrp1 discrp,-total_pot total_pot";
        $criteria->addCondition("transfer_item_id = :transfer_item_id");
        $criteria->params = array(':transfer_item_id'=>$_POST['transfer_item_id']);
        $model = TransferItemDetails::model()->findAll($criteria);
        $total = TransferItemDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}