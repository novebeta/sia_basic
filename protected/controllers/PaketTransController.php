<?php
class PaketTransController extends GxController
{
    public function actionCreate()
    {
        $model = new PaketTrans;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['PaketTrans'][$k] = $v;
            }
            $model->attributes = $_POST['PaketTrans'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->paket_trans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PaketTrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['PaketTrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PaketTrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->paket_trans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->paket_trans_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PaketTrans')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['salestrans_id'])) {
            $criteria->addCondition('salestrans_id = :salestrans_id');
            $param[':salestrans_id'] = $_POST['salestrans_id'];
            /** @var $sales Salestrans */
            $sales = Salestrans::model()->findByPk($_POST['salestrans_id']);
            if ($sales->type_ == -1) {
                $criteria->select = "npt.paket_trans_id,npt.paket_id,npt.salestrans_id,-npt.qty qty";
                $criteria->alias = "npt";
            }
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = PaketTrans::model()->findAll($criteria);
        $total = PaketTrans::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionItem()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            $qty = get_number($_POST['qty']);
            $paket_id = $_POST['paket_id'];
            /* @var $paket Paket */
            $paket = Paket::model()->findByPk($paket_id);
            $paket_trans_id = $this->generate_uuid();
            $sales_detil = array();
            $total_paket = 0;
            $new_details = array();
            foreach ($detils as $detil) {
                /* @var $barang Barang */
                $barang = Barang::model()->findByPk($detil['barang_id']);
                $price = $barang->get_price(STOREID);
                $vat = $barang->get_tax(STOREID);
                $total_paket += $price;
                $detil['price'] = $price;
                $detil['vat'] = $vat;
                $new_details[] = $detil;
            }
            $total_disc = $total_paket - $paket->proposed_price;
            $disc = ($total_disc / $total_paket) * 100;
            foreach ($new_details as $detil) {
//                $disc = get_number($detil['disc']);
                $price = $detil['price'];
                $vat = $detil['vat'];
                $bruto = round($price * $qty, 2);
                $discrp = round(($disc / 100) * $bruto, 2);
//                $discrp = ($disc / 100) * $bruto;
                $totalpot = $discrp;
                $total_with_disc = $bruto - $totalpot;
                $total = $bruto - $discrp;
                $vatrp = round($total_with_disc * $vat, 2);
                $_detil_trans_item = array(
                    'paket_trans_id' => $paket_trans_id,
                    'paket_details_id' => $detil['paket_details_id'],
                    'barang_id' => $detil['barang_id'],
                    'qty' => $qty,
                    'disc' => $disc,
                    'discrp' => $discrp,
                    'ketpot' => $detil['ketpot'],
                    'vat' => $vat,
                    'vatrp' => $vatrp,
                    'bruto' => $bruto,
                    'total' => $total,
                    'total_pot' => $totalpot,
                    'price' => $price,
                    'disc_name' => $paket->paket_name,
                    'disc1' => 0,
                    'discrp1' => 0,
                );
                $sales_detil[] = $_detil_trans_item;
            }
            $this->renderJsonArr($sales_detil);
        }
    }
}