<?php
return array(
    'basePath' => dirname(__file__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'PT. BIANTI ANUGRAH TERINDAH',
    'theme' => 'extjs',
    'preload' => array('log'),
    'sourceLanguage' => 'xx',
    'language' => 'en',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'application.vendors.*'
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'admin',
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'ext.giix-core',
            ),
        ),
    ),
    'components' => array(
        'CGridViewPlus' => array(
            'class' => 'components.CGridViewPlus',
        ),
        'user' => array(
            'loginUrl' => array('login'),
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<action:(contact|login|logout|GetDateTime)>/*' => 'site/<action>',
                '<controller:\w+>/<id:[a-zA-Z0-9-]  >' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:[a-zA-Z0-9-]>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=ptkita;port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'nscc_',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'session' => array(
            'sessionName' => 'pos-nsc',
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable' => false,
            'connectionID' => 'db',
            'sessionTableName' => 'nscc_session',
            'useTransparentSessionID' => isset($_POST['PHPSESSID']) ? true : false,
            'autoStart' => 'false',
            'cookieMode' => 'only',
            'timeout' => 10800
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                    'filter' => 'CLogFilter',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 10
                ),
            ),
        ),
//        'log' => array(
//            'class' => 'CLogRouter',
//            'routes' => array(
//                array(
//                    'class' => 'CPhpMailerLogRoute',
//                    'levels' => 'error',
//                    'emails' => 'nscc.error@gmail.com',
//                    'sentFrom' => 'nscc.error@gmail.com',
//                    'subject' => 'Error ' . STOREID . ' ' . date('dmYHis'),
//                    'config' => array(
//                        'SMTP',
//                        'Host' => "smtp.gmail.com",
//                        'SMTPAuth' => true, // enable SMTP authentication
//                        'SMTPSecure' => "tls", // sets the prefix to the server
//                        'Port' => 587,
//                        'Username' => "nscc.error@gmail.com", // GMAIL username
//                        'Password' => 'zaq!@#$%',
//                        'CharSet' => "UTF-8",
//                    )
//                ),
//            ),
//        ),
        'aes256' => array(
            'class' => 'application.extensions.aes256.Aes256',
            'privatekey_32bits_hexadecimal' => '9C9413969BD7524F4CD07FECDEE0ED185D1CB08A16DB4D56B14FF951A64E96AC',
        ),
        'Smtpmail' => array(
            'class' => 'application.extensions.smtpmail.PHPMailer',
            'Host' => "smtp.gmail.com",
            'Username' => '',
            'Password' => '',
            'Mailer' => 'smtp',
            'Port' => 587,
            'SMTPAuth' => true,
            'SMTPSecure' => 'tls',
        ),
    ),
    'params' => array(
        'Username' => '',
        'Password' => '',
        'system_title' => 'Point Of Sales',
        'system_subtitle' => '',
        'nama_perusahaan' => 'PT. BIANTI ANUGRAH TERINDAH',
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
    ),
);
