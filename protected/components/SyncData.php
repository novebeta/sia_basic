<?php
class SyncData
{
    /*
     *  Table yang tidak di sync adalah :
     *
     *  nscc_employee
     *  nscc_absen_trans
     *  nscc_nscc_tipe_absen
     *
     * */
    private $result;
    private $username;
    private $password;
    private $status_error;
    private $skip_primary;
    private $body_message;
    public function sync()
    {
//        $this->skip_primary = array(
//            'Payment',
//            'StockMoves',
//            'BankTrans',
//            'TenderDetails',
//            'TransferItemDetails',
//            'GlTrans',
//            'Comments',
//            'BeautyServices',
//            'PrintzDetails',
//            'Refs',
//            'Jual',
//            'GrupAttr'
//        );
        Yii::app()->db->createCommand("
        ALTER TABLE nscc_tipe_barang AUTO_INCREMENT = 1;
        ALTER TABLE nscc_trans_tipe AUTO_INCREMENT = 1;")
            ->execute(array());
        $this->username = yiiparam('Username');
        $this->password = yiiparam('Password');
        app()->db->autoCommit = false;
        if (is_connected(NARSDOMAIN, NARSPORT)) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_cust_nars();
                self::upload_trans_nars();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::echo_log('NARS_ERROR', 'NARS', $ex->getMessage());
            }
        } else {
            self::echo_log('NARS_ERROR', 'NARS', 'No connection');
        }
        if (is_connected('imap.gmail.com', 993) && is_connected('smtp.gmail.com', 587)) {
            self::download_global();
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_global();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::echo_log('GLOBAL_UP_ERROR', 'GLOBAL', $ex->getMessage());
            }
            self::download_master();
            if (BROADCAST) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::broadcast_master();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
                    self::echo_log('BROADCAST_UP_ERROR', 'MASTER', $ex->getMessage());
                }
            }
            if (TOSTOREHO) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::upload_master_all();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
                    self::echo_log('MASTERHO_UP_ERROR', 'MASTER', $ex->getMessage());
                }
            }
            self::download_transaksi();
            if (TOSTOREHO) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::upload_transaksi();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
                    self::echo_log('TRANS_UP_ERROR', 'TRANSACTION', $ex->getMessage());
                }
            }
            if (!HEADOFFICE) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::remainder();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
                    self::echo_log('SEND_REMAINDER', 'REMAINDER', $ex->getMessage());
                }
            }
        } else {
            self::echo_log('EMAIL', 'IMAP', 'No connection');
        }
        app()->db->autoCommit = true;
        Yii::app()->db->createCommand("DELETE FROM nscc_sync WHERE DATEDIFF(NOW(),tdate) > :days")
            ->execute(array(":days" => DAYSBEFOREDELSYNC));
    }
    private function upload_cust_nars()
    {
        $cust = Customers::get_backup();
        if (count($cust) == 0) {
//            $this->result['upload_cust_nars'] = 'nothing to upload';
            self::echo_log('NARS', 'UPLOAD_CUST_NARS', 'No connection');
            return;
        }
        $url = NARSURL;
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'pasien',
            'data' => CJSON::encode($cust)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'ignore_errors' => true,
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_customers SET log = 1 WHERE log = 0
           ")->execute();
//            $this->result['upload_cust_nars'] = 'success upload ' . number_format($r) . ' records';
            self::echo_log('NARS', 'UPLOAD_CUST_NARS', 'success upload ' . number_format($r) . ' records');
            return;
        }
//        $this->result['upload_cust_nars'] = 'failed upload records';
        self::echo_log('upload_cust_nars', 'NARS', 'failed upload records');
    }
    private function upload_trans_nars()
    {
        $sales = Salestrans::get_backup();
        if (count($sales) == 0) {
//            $this->result['upload_trans_nars'] = 'nothing to upload';
            self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'nothing to upload');
            return;
        }
        $url = NARSURL;
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'transaksi',
            'data' => CJSON::encode($sales)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_salestrans SET log = 1 WHERE log = 0
           ")->execute();
//            $this->result['upload_trans_nars'] = 'success upload ' . number_format($r) . ' records';
            self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'success upload ' . number_format($r) . ' records');
            return;
        }
//        $this->result['upload_trans_nars'] = 'failed upload records';
        self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'failed upload records');
    }
    private function upload_global()
    {
        $global = array(
            'SecurityRoles',
            'Gol',
            'TransTipe',
            'Customers',
            'StatusCust',
            'ChartMaster',
            'Supplier',
            'Kategori',
            'Negara',
            'Provinsi',
            'Kota',
            'Kecamatan',
            'Store',
            'Card',
            'Barang',
            'Grup',
            'KategoriClinical',
            'BarangClinical',
            'TipeTransClinical',
            'Info',
            'Wilayah',
            'TipeBarang',
            'PointTrans',
            'Event',
            'Souvenir',
            'TransTipe',
            'PromoGeneric'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        foreach ($global as $modelName) {
            $body = array(
                'from' => STOREID,
            );
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
                if ($modelName == "Paket") {
                }
            }
            if (count($upload) == 0) {
//                $this->result['upload_global'][$modelName] = 'nothing to upload';
                self::echo_log('UPLOAD_GLOBAL', $modelName, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $body['date'] = $time;
            $body['paket'] = false;
            $body['content'] = array($content);
            $status = mailsend($to, $to, $subject,
                json_encode($body, JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_global'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_GLOBAL', $modelName,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new Sync;
                $sync->type_ = "G";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = STOREGLOBAL;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_global'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_GLOBAL', $modelName, 'failed upload records');
            }
        }
        $body = array(
            'from' => STOREID,
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $paket = Paket::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($paket != null) {
            $upload['Paket'] = $paket;
            $modelName = 'Paket';
            $content['Paket'] = number_format(count($paket)) . ' record';
            $paketDetails = array();
            $paketBarang = array();
            /** @var $row Paket */
            foreach ($paket as $row) {
                if (!empty($row->paketDetails)) {
                    $paketDetails = array_merge($paketDetails, $row->paketDetails);
                    foreach ($row->paketDetails as $pktbrg) {
                        if (!empty($pktbrg->paketBarangs)) {
                            $paketBarang = array_merge($paketBarang, $pktbrg->paketBarangs);
                        }
                    }
                }
//                $body['id'] = $row->paket_id;
                $id[] = $row->paket_id;
                if (!empty($paketDetails)) {
                    $upload['PaketDetails'] = $paketDetails;
                    $content['PaketDetails'] = number_format(count($paketDetails)) . ' record';
                }
                if (!empty($paketBarang)) {
                    $upload['PaketBarang'] = $paketBarang;
                    $content['PaketBarang'] = number_format(count($paketBarang)) . ' record';
                }
                $to = yiiparam('Username');
                $time = date('Y-m-d H:i:s');
                $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
                $body['model'] = $modelName;
                $body['paket'] = true;
                $body['date'] = $time;
                $body['id'] = $id;
                $body['content'] = array($content);
                $status = mailsend($to, $to, $subject,
                    json_encode($body, JSON_PRETTY_PRINT),
                    bzcompress(Encrypt(CJSON::encode($upload)), 9)
                );
                if ($status == "OK") {
                    foreach ($paket as $model) {
                        $model->up = 1;
                        if (!$model->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => $modelName)) . CHtml::errorSummary($model));
                        }
                    }
                    self::echo_log('UPLOAD_GLOBAL', $modelName,
                        'success upload ' . number_format(count($paket)) . ' records');
                    $sync = new Sync;
                    $sync->type_ = "G";
                    $sync->subject = $subject;
                    if (!$sync->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                    }
                    $log = new UploadLog;
                    $log->target = 'GLOBAL';
                    $log->upload_time = $time;
                    $log->content_ = CJSON::encode($upload);
                    $log->subject = $subject;
                    if (!$log->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                    }
                } else {
                    self::echo_log('UPLOAD_GLOBAL', $modelName, 'failed upload records');
                }
            }
        }
    }
    private function upload_master_cabang($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array(
            'SysPrefs',
            'Users',
            'Bank',
            'Diskon',
            'Price',
            'Dokter',
            'Beauty',
            'Beli',
            'Jual',
            'GrupAttr'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->addCondition("store = :store");
        $criteria->params = array(':store' => $from);
        foreach ($master as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_master_cabang'][$modelName][$target] = 'nothing to upload';
                self::echo_log('UPLOAD_MASTER_CABANG', $modelName . "-" . $target, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = 'M' . $target . '-' . sha1($from . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => $from,
                    'date' => $time,
                    'paket' => false,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0 AND store = :store")->execute(array(':store' => $from));
                foreach ($tables as $model) {
                    $model->up = 1;
//                    $model->store = $from;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_master'][$modelName][$target] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new Sync;
                $sync->type_ = "M";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'M' . $target;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_master'][$modelName][$target] = 'failed upload records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target, 'failed upload records');
            }
        }
    }
    private function upload_master_all($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array(
            'SysPrefs',
            'Users',
            'Bank',
            'Barang',
            'Price',
            'Dokter',
            'Beauty',
            'Beli',
            'Jual',
            'GrupAttr'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        foreach ($master as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_master_all'][$modelName][$target] = 'nothing to upload';
                self::echo_log('UPLOAD_MASTER_ALL', $modelName . "-" . $target, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = 'M' . $target . '-' . sha1($from . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => $from,
                    'date' => $time,
                    'paket' => false,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_master_all'][$modelName][$target] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_MASTER_ALL', $modelName . "-" . $target,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new Sync;
                $sync->type_ = "M";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'M' . $target;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_master'][$modelName][$target] = 'failed upload records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target, 'failed upload records');
            }
        }
    }
    /**
     * @throws Exception
     */
    private function upload_transaksi()
    {
        $trans = array(
            'Refs',
            'Comments',
            'GlTrans',
            'StockMoves',
            'BankTrans',
            'StockMovesClinical',
            'Mgm',
            'Msd',
            'SouvenirTrans',
            'SouvenirOut'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $to = yiiparam('Username');
        foreach ($trans as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_transaksi'][$modelName] = 'nothing to upload';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'nothing to upload');
                continue;
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => false,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Tender ==============================================================================================================
        /** @var $tender Tender[] */
        $tender = Tender::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($tender != null) {
            $upload['Tender'] = $tender;
            $modelName = 'Tender';
            $content['Tender'] = number_format(count($tender)) . ' record';
            $tenderDetails = array();
            $printz = array();
            $printzDetails = array();
            foreach ($tender as $row) {
//                $row = new Tender;
                $id[] = $row->tender_id;
                if (!empty($row->tenderDetails)) {
                    $tenderDetails = array_merge($tenderDetails, $row->tenderDetails);
                }
                if (!empty($row->printzs)) {
                    $printz = array_merge($printz, $row->printzs);
                }
                foreach ($row->printzs as $row_printz) {
                    if (!empty($row_printz->printzDetails)) {
                        $printzDetails = array_merge($printzDetails, $row_printz->printzDetails);
                    }
                }
            }
            if (count($tenderDetails) > 0) {
                $upload['TenderDetails'] = $tenderDetails;
                $content['TenderDetails'] = number_format(count($tenderDetails)) . ' record';
            }
            if (!empty($printz)) {
                $upload['Printz'] = $printz;
                $content['Printz'] = number_format(count($printz)) . ' record';
            }
            if (!empty($printzDetails)) {
                $upload['PrintzDetails'] = $printzDetails;
                $content['PrintzDetails'] = number_format(count($printzDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tender as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tender)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($tender)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Tender ==============================================================================================================
// TransferItem ========================================================================================================
        /** @var $transferItem TransferItem[] */
        $transferItem = TransferItem::model()->findAll($criteria);
//        $tender = new TransferItem;
        $upload = array();
        $content = array();
        $id = array();
        if ($transferItem != null) {
            $modelName = 'TransferItem';
            $upload['TransferItem'] = $transferItem;
            $content['TransferItem'] = number_format(count($transferItem)) . ' record';
            $transferItemDetails = array();
            foreach ($transferItem as $row) {
                $id[] = $row->transfer_item_id;
                if ($row->transferItemDetails != null) {
                    $transferItemDetails = array_merge($transferItemDetails, $row->transferItemDetails);
                }
            }
            if (count($transferItemDetails) > 0) {
                $upload['TransferItemDetails'] = $transferItemDetails;
                $content['TransferItemDetails'] = number_format(count($transferItemDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($transferItem as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($transferItem)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($transferItem)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// TransferItem ========================================================================================================
// Kas =================================================================================================================
        /* @var $kas Kas[] */
        $kas = Kas::model()->findAll($criteria);
//        $kas = new Kas;
        $upload = array();
        $content = array();
        $id = array();
        if ($kas != null) {
            $modelName = 'Kas';
            $upload['Kas'] = $kas;
            $content['Kas'] = number_format(count($kas)) . ' record';
            $pelunasanUtangDetails = array();
            foreach ($kas as $row) {
                $id[] = $row->kas_id;
                if (count($row->kasDetails) > 0) {
                    $pelunasanUtangDetails = array_merge($pelunasanUtangDetails, $row->kasDetails);
                }
            }
            if (count($pelunasanUtangDetails) > 0) {
                $upload['KasDetail'] = $pelunasanUtangDetails;
                $content['KasDetail'] = number_format(count($pelunasanUtangDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($kas as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($kas)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($kas)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Kas =================================================================================================================
// Salestrans ==========================================================================================================
        /** @var $salestrans Salestrans[] */
        $salestrans = Salestrans::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($salestrans != null) {
            $modelName = 'Salestrans';
            $upload['Salestrans'] = $salestrans;
            $content['Salestrans'] = number_format(count($salestrans)) . ' record';
            $salestransDetails = array();
            $payments = array();
            $paketTrans = array();
            foreach ($salestrans as $row) {
                $id[] = $row->salestrans_id;
                if (count($row->salestransDetails) > 0) {
                    $salestransDetails = array_merge($salestransDetails, $row->salestransDetails);
                }
                if (count($row->payments) > 0) {
                    $payments = array_merge($payments, $row->payments);
                }
                if (count($row->paketTrans) > 0) {
                    $paketTrans = array_merge($paketTrans, $row->paketTrans);
                }
            }
            if (count($salestransDetails) > 0) {
                $upload['SalestransDetails'] = $salestransDetails;
                $content['SalestransDetails'] = number_format(count($salestransDetails)) . ' record';
            }
            if (count($payments) > 0) {
                $upload['Payment'] = $payments;
                $content['Payment'] = number_format(count($payments)) . ' record';
            }
            if (count($paketTrans) > 0) {
                $upload['PaketTrans'] = $paketTrans;
                $content['PaketTrans'] = number_format(count($paketTrans)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($salestrans as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($salestrans)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Salestrans ==========================================================================================================
// BeautyServices ======================================================================================================
        /** @var $pelunasanUtang BeautyServices[] */
        $pelunasanUtang = BeautyServices::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($pelunasanUtang != null) {
            $modelName = 'BeautyServices';
            $upload['BeautyServices'] = $pelunasanUtang;
            foreach ($pelunasanUtang as $row) {
                $id[] = $row->nscc_beauty_services_id;
            }
            $content['BeautyServices'] = number_format(count($pelunasanUtang)) . ' record';
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($pelunasanUtang as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($pelunasanUtang)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($pelunasanUtang)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// BeautyServices ======================================================================================================
// PelunasanUtang ======================================================================================================
        /** @var $pelunasanUtang PelunasanUtang[] */
        $pelunasanUtang = PelunasanUtang::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($pelunasanUtang != null) {
            $modelName = 'PelunasanUtang';
            $upload['PelunasanUtang'] = $pelunasanUtang;
            $content['PelunasanUtang'] = number_format(count($pelunasanUtang)) . ' record';
            $pelunasanUtangDetails = array();
            foreach ($pelunasanUtang as $row) {
                $id[] = $row->pelunasan_utang_id;
                if ($row->pelunasanUtangDetils != null) {
                    $pelunasanUtangDetails = array_merge($pelunasanUtangDetails, $row->pelunasanUtangDetils);
                }
            }
            if (count($pelunasanUtangDetails) > 0) {
                $upload['PelunasanUtangDetil'] = $pelunasanUtangDetails;
                $content['PelunasanUtangDetil'] = number_format(count($pelunasanUtangDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($pelunasanUtang as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($pelunasanUtang)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($pelunasanUtang)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// PelunasanUtang ======================================================================================================
        /** @var $cTrans ClinicalTrans[] */
        $cTrans = ClinicalTrans::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($cTrans != null) {
            $modelName = 'ClinicalTrans';
            $upload['ClinicalTrans'] = $cTrans;
            $content['ClinicalTrans'] = number_format(count($cTrans)) . ' record';
            $cTransDetails = array();
            foreach ($cTrans as $row) {
                $id[] = $row->clinical_trans_id;
                if (count($row->clinicalTransDetails) > 0) {
                    $cTransDetails = array_merge($cTransDetails, $row->clinicalTransDetails);
                }
            }
            if (count($cTransDetails) > 0) {
                $upload['ClinicalTransDetail'] = $cTransDetails;
                $content['ClinicalTransDetail'] = number_format(count($cTransDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
                foreach ($cTrans as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($cTrans)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
        /** @var $prod Produksi[] */
        $prod = Produksi::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($prod != null) {
            $modelName = 'Produksi';
            $upload['Produksi'] = $prod;
            $content['Produksi'] = number_format(count($prod)) . ' record';
            $prodDetails = array();
            foreach ($prod as $row) {
                $id[] = $row->produksi_id;
                if (count($row->produksiDetils) > 0) {
                    $prodDetails = array_merge($prodDetails, $row->produksiDetils);
                }
            }
            if (count($prodDetails) > 0) {
                $upload['ProduksiDetil'] = $prodDetails;
                $content['ProduksiDetil'] = number_format(count($prodDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
                foreach ($prod as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($prod)) . ' records');
                $sync = new Sync;
                $sync->type_ = "T";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
    }
    private function broadcast_master()
    {
        $store = Store::model()->findAll('store_kode <> :store_kode',
            array(':store_kode' => STOREID));
        foreach ($store as $target) {
            self::upload_master_cabang(STOREID, $target->store_kode);
        }
    }
    private function download_global()
    {
        $this->body_message = "";
        $json = self::download_email(STOREGLOBAL, 'G');
        if ($json === false) {
            self::echo_log('DOWNLOAD_GLOBAL', 'failed download', $this->status_error);
            return;
        } else {
            foreach ($json as $email) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $body = json_decode($email['message'], true);
                    if ($body['paket']) {
                        switch ($body['model']) {
                            case "Paket" :
                                foreach ($body['id'] as $row) {
                                    Yii::app()->db->createCommand("SET @DISABLE_TRIGGERS=1;
                                    DELETE np,npd,npb FROM nscc_paket AS np
                                    LEFT JOIN nscc_paket_details AS npd ON npd.paket_id = np.paket_id
                                    LEFT JOIN nscc_paket_barang AS npb ON npb.paket_details_id = npd.paket_details_id
                                    WHERE np.paket_id = :paket_id;
                                    SET @DISABLE_TRIGGERS=NULL;")
                                        ->execute(array(':paket_id' => $row));
                                }
                                break;
                        }
                    }
                    foreach ($email['attachments'] as $attachment) {
                        $data = json_decode($attachment, true);
                        foreach ($data as $k => $v) {
                            foreach ($v as $row) {
                                $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                                $pkval = $row[$primarykey];
                                $model = CActiveRecord::model($k)->findByPk($pkval);
                                if ($model == null) {
                                    $model = new $k;
                                }
                                $model->attributes = $row;
                                if (!$model->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($model));
                                }
                                if (CActiveRecord::model($k)->tableSchema->getColumn('up') != null) {
                                    $model->up = 1;
                                    if (!$model->save()) {
                                        throw new Exception(t('save.model.fail', 'app',
                                                array('{model}' => $k)) . CHtml::errorSummary($model));
                                    }
                                }
                            }
                            self::echo_log('DOWNLOAD_GLOBAL', $k,
                                'success download ' . number_format(count($v)) . ' records');
                        }
                    }
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
                    self::echo_log('GLOBAL_DOWN_ERROR', 'GLOBAL', $ex->getMessage());
                }
            }
            self::echo_log('GLOBAL_DOWN_FINISH', 'GLOBAL', '-------------');
        }
    }
    private function download_master()
    {
        $json = self::download_email(STOREID, 'M');
        if ($json === false) {
            self::echo_log('DOWNLOAD_MASTER', 'failed download', $this->status_error);
            return;
        } else {
            foreach ($json as $email) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    foreach ($email['attachments'] as $attachment) {
                        $data = json_decode($attachment, true);
                        foreach ($data as $k => $v) {
                            foreach ($v as $row) {
                                $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                                $pkval = $row[$primarykey];
                                $model = CActiveRecord::model($k)->findByPk($pkval);
                                if ($model == null) {
                                    $model = new $k;
                                }
                                $model->attributes = $row;
                                if (!$model->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($model));
                                }
                                if (CActiveRecord::model($k)->tableSchema->getColumn('up') != null) {
                                    $model->up = MARKAFTERDOWNLOAD ? 1 : 0;
                                    $model->attributes = $row;
                                    if (!$model->save()) {
                                        throw new Exception(t('save.model.fail', 'app',
                                                array('{model}' => $k)) . CHtml::errorSummary($model));
                                    }
                                }
                            }
                            self::echo_log('DOWNLOAD_MASTER', $k,
                                'success download ' . number_format(count($v)) . ' records');
                        }
                    }
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
                    self::echo_log('DOWNLOAD_MASTER', 'MASTER', $ex->getMessage());
                }
            }
            self::echo_log('DOWNLOAD_MASTER', 'MASTER', '-------------');
        }
    }
    private function download_transaksi()
    {
        $this->body_message = "";
        $json = self::download_email(STOREID, 'T');
        if ($json === false) {
            self::echo_log('DOWNLOAD_TRANSAKSI', 'failed download', $this->status_error);
            return;
        } else {
            foreach ($json as $email) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $body = json_decode($email['message'], true);
                    if ($body['paket']) {
                        switch ($body['model']) {
                            case "Tender" :
                                foreach ($body['id'] as $row) {
                                    Yii::app()->db->createCommand("DELETE nt,ntd,np,npd
                            FROM nscc_tender AS nt
                            LEFT JOIN nscc_tender_details AS ntd ON ntd.tender_id = nt.tender_id
                            LEFT JOIN nscc_printz AS np ON np.tender_id = nt.tender_id
                            LEFT JOIN nscc_printz_details AS npd ON npd.prntz_id = np.prntz_id
                            WHERE nt.tender_id = :tender_id")
                                        ->execute(array(':tender_id' => $row));
                                }
                                break;
                            case "TransferItem" :
                                foreach ($body['id'] as $row) {
                                    Yii::app()->db->createCommand("DELETE nti,ntid
                            FROM nscc_transfer_item AS nti
                            LEFT JOIN nscc_transfer_item_details AS ntid ON ntid.transfer_item_id = nti.transfer_item_id
                            WHERE nti.transfer_item_id= :transfer_item_id")
                                        ->execute(array(':transfer_item_id' => $row));
                                }
                                break;
                            case "Kas" :
                                foreach ($body['id'] as $row) {
                                    Yii::app()->db->createCommand("DELETE nk,nkd
                            FROM nscc_kas AS nk
                            LEFT JOIN nscc_kas_detail AS nkd ON nkd.kas_id = nk.kas_id
                            WHERE nk.kas_id= :kas_id")
                                        ->execute(array(':kas_id' => $row));
                                }
                                break;
                            case "Salestrans" :
                                foreach ($body['id'] as $row) {
                                    Yii::app()->db->createCommand("DELETE npt,np,nsd,ns
                            FROM nscc_salestrans AS ns
                            LEFT JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
                            LEFT JOIN nscc_payment AS np ON np.salestrans_id = ns.salestrans_id
                            LEFT JOIN nscc_paket_trans AS npt ON npt.salestrans_id = ns.salestrans_id
                            WHERE ns.salestrans_id= :salestrans_id")
                                        ->execute(array(':salestrans_id' => $row));
                                }
                                break;
                            case "BeautyServices" :
                                foreach ($body['id'] as $row) {
                                    Yii::app()->db->createCommand("DELETE FROM nscc_beauty_services AS nbs
                            WHERE nbs.nscc_beauty_services_id = :nscc_beauty_services_id")
                                        ->execute(array(':nscc_beauty_services_id' => $row));
                                }
                                break;
                            case "PelunasanUtang" :
                                foreach ($body['id'] as $row) {
                                    Yii::app()->db->createCommand("DELETE npud,npu
                            FROM nscc_pelunasan_utang AS npu
                            LEFT JOIN nscc_pelunasan_utang_detil AS npud ON npud.pelunasan_utang_id = npu.pelunasan_utang_id
                            WHERE npud.pelunasan_utang_id = :pelunasan_utang_id")
                                        ->execute(array(':pelunasan_utang_id' => $row));
                                }
                                break;
                        }
                    }
                    foreach ($email['attachments'] as $attachment) {
                        $data = json_decode($attachment, true);
                        foreach ($data as $k => $v) {
                            foreach ($v as $row) {
                                $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                                $pkval = $row[$primarykey];
                                $model = CActiveRecord::model($k)->findByPk($pkval);
                                if ($model == null) {
                                    $model = new $k;
                                }
                                $model->attributes = $row;
                                if (!$model->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($model));
                                }
                                if (CActiveRecord::model($k)->tableSchema->getColumn('up') != null) {
                                    $model->up = MARKAFTERDOWNLOAD ? 1 : 0;
                                    $model->attributes = $row;
                                    if (!$model->save()) {
                                        throw new Exception(t('save.model.fail', 'app',
                                                array('{model}' => $k)) . CHtml::errorSummary($model));
                                    }
                                }
                            }
//                $this->result['download_transaksi'][$k] = 'success download ' . number_format(count($v)) . ' records';
                            self::echo_log('DOWNLOAD_TRANSAKSI', $k,
                                'success download ' . number_format(count($v)) . ' records');
                        }
                    }
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
                    self::echo_log('TRANS_DOWN_ERROR', 'TRANSACTION', $ex->getMessage());
                }
            }
            self::echo_log('TRANS_DOWN_ERROR', 'TRANSACTION', '-------------');
        }
    }
    private function download_email($imapmainbox, $type_ = null)
    {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $messagestatus = "ALL";
            $imapaddress = IMAPADDRESS;
            $hostname = $imapaddress . ($type_ == 'G' ? '' : $type_) . $imapmainbox;
            $connection = @imap_open($hostname, $this->username, $this->password);
            if ($connection === false) {
                $this->status_error = 'Cannot connect to Gmail: ' . imap_last_error();
                throw new Exception('Cannot connect to Gmail: ' .imap_last_error());
            }
            $check = imap_check($connection);
            if ($check->Nmsgs == 0) {
                @imap_close($connection);
                throw new Exception('No Message.');
            }
            $emails = imap_search($connection, $messagestatus);
            $all_emails = array();
            if ($emails) {
                foreach ($emails as $email_number) {
                    $email_rows = array();
                    $json = array();
                    $header = imap_fetch_overview($connection, $email_number, 0);
                    if (empty($header)) {
                        self::echo_log('DONWLOAD_EMAIL', 'HEADER_EMPTY', var_export($header, true));
                        continue;
                    }
                    $subject = $header[0]->subject;
                    $date = strtotime($header[0]->date);
                    $date2 = time();
                    $subTime = $date2 - $date;
                    $h = $subTime / (60 * 60 * 24);
                    if ($h > DAYSBEFOREDELSYNC) {
                        imap_delete($connection, $email_number, 1);
                        continue;
                    }
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("type_ = :type_ AND subject = :subject");
                    $criteria->params = array(':type_' => $type_, ':subject' => $subject);
                    $count = Sync::model()->count($criteria);
                    if ($count > 0) {
                        continue;
                    }
                    $email_rows['subject'] = $subject;
                    $message = imap_fetchbody($connection, $email_number, 1.1);
                    if ($message == "") {
                        $message = imap_fetchbody($connection, $email_number, 1);
                    }
                    $email_rows['message'] = $message;
                    $structure = imap_fetchstructure($connection, $email_number);
                    $attachments = array();
                    if (isset($structure->parts) && count($structure->parts)) {
                        for ($i = 0; $i < count($structure->parts); $i++) {
                            $attachments[$i] = array(
                                'is_attachment' => false,
                                'filename' => '',
                                'name' => '',
                                'attachment' => ''
                            );
                            if ($structure->parts[$i]->ifdparameters) {
                                foreach ($structure->parts[$i]->dparameters as $object) {
                                    if (strtolower($object->attribute) == 'filename') {
                                        $attachments[$i]['is_attachment'] = true;
                                        $attachments[$i]['filename'] = $object->value;
                                    }
                                }
                            }
                            if ($structure->parts[$i]->ifparameters) {
                                foreach ($structure->parts[$i]->parameters as $object) {
                                    if (strtolower($object->attribute) == 'name') {
                                        $attachments[$i]['is_attachment'] = true;
                                        $attachments[$i]['name'] = $object->value;
                                    }
                                }
                            }
                            if ($attachments[$i]['is_attachment']) {
                                $attachments[$i]['attachment'] = imap_fetchbody($connection, $email_number, $i + 1);
                                if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                    $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                                } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                    $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                                }
                            }
                        }
                    }
                    if (count($attachments) != 0) {
                        foreach ($attachments as $at) {
                            if ($at['is_attachment'] == 1) {
                                $json[] = Decrypt(bzdecompress($at['attachment']));
                                $sync = new Sync;
                                $sync->type_ = $type_;
                                $this->body_message = $message;
                                $sync->subject = $subject;
                                $sync->message = $message;
                                if (!$sync->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                                }
                            }
                        }
                        $email_rows['attachments'] = $json;
                    }
                    $all_emails[] = $email_rows;
                }
            }
            imap_expunge($connection);
            imap_close($connection);
            $transaction->commit();
            return $all_emails;
        } catch (Exception $ex) {
            $transaction->rollback();
            self::echo_log('DOWNLOAD EMAIL', 'GLOBAL', $ex->getMessage());
            return false;
        }
    }
    private function echo_log($cat, $model, $note)
    {
        echo json_encode(
                array(
                    'time' => date("Y-m-d H:i:s"),
                    'event' => $cat,
                    'model' => $model,
                    'note' => $note
                )) . PHP_EOL;
    }
    private function remainder()
    {
        $rec = app()->db->createCommand("SELECT nti.store,ns.supplier_name,
        DATE_FORMAT(nti.tgl,'%d %b %Y') Date,DATE_FORMAT(nti.tgl_jatuh_tempo,'%d %b %Y') DueDate,
        DATEDIFF(nti.tgl_jatuh_tempo, NOW()) remainDays,nti.doc_ref,nti.doc_ref_other,nti.total,
        nti.total - (IF (SUM(npud.kas_dibayar) IS NULL,0,SUM(npud.kas_dibayar))) AS 'Remain'
        FROM nscc_transfer_item AS nti
        LEFT JOIN nscc_pelunasan_utang_detil AS npud ON nti.transfer_item_id = npud.transfer_item_id
        INNER JOIN nscc_supplier AS ns ON nti.supplier_id = ns.supplier_id
        WHERE nti.lunas = 0 AND DATEDIFF(nti.tgl_jatuh_tempo, NOW()) < :start
        GROUP BY nti.transfer_item_id
        HAVING Remain > 0");
        $result = $rec->queryAll(true, array(':start' => REMAINDER_START));
        if (count($result) == 0) {
            return;
        }
        $stat = SysPrefs::model()->find('name_ = :name AND store =:store',
            array(':name' => 'last_remainder', ':store' => STOREID));
        $last_remainder = $stat->value_;
        $dStart = new DateTime($last_remainder);
        $dEnd = new DateTime();
        $dDiff = $dStart->diff($dEnd);
        //echo $dDiff->format('%R'); // use for point out relation: smaller/greater
        $days = $dDiff->days;
        if ($days < REMAINDER_INTERVAL) {
            return;
        }
        $sum_Remain = array_sum(array_column($result, 'Remain'));
        $sum_total = array_sum(array_column($result, 'total'));
        $body = "<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
</head>
<body>
<p>Dear Bapak Ferro,</p>
<p>Berikut data tagihan yang mendekati jatuh tempo.</p>
<table style='font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border: 1px solid #666666;' ><tr>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Supplier</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Date</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Due Date</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Remain Days</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Invoice No.</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Total</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Remain</th></tr>";
        foreach ($result as $row) {
            $body .= "<tr>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['supplier_name'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['Date'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['DueDate'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . $row['remainDays'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['doc_ref_other'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($row['total'], 2) . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($row['Remain'],
                    2) . "</td></tr>";
        }
        $body .= "<tr>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>TOTAL</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' colspan='4'></td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($sum_total, 2) . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($sum_Remain, 2) . "</td></tr>";
        $body .= "</table><p>Terima Kasih</p>
</body>
</html>";
//        $body = $this->render('Remainder', array('dp' => $dataProvider, 'total_remain' => $sum_Remain), true);
        $send = mailsend_remainder(REMAINDER_EMAIL, yiiparam('Username'),
            'REMAINDER TAGIHAN HUTANG NWIS ' . STOREID . ' ' . date('d/m/Y'), $body);
        if ($send == "OK") {
            $stat->value_ = date('Y-m-d');
            $stat->save();
            self::echo_log('REMAINDER', 'SEND_REMAINDER',
                'success remainder ' . number_format(count($rec)) . ' records');
//            $this->result['send_remainder'] = 'success remainder ' . number_format(count($rec)) . ' records';
        } else {
//            $this->result['send_remainder'] = 'failed remainder';
            self::echo_log('REMAINDER', 'SEND_REMAINDER', 'failed remainder');
        }
    }
} 