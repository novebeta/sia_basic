<?php
/**
 * Created by PhpStorm.
 * User: MASTER
 * Date: 7/14/14
 * Time: 8:13 AM
 */

class PrintCashOut extends BasePrint{
    private $s;
    function PrintCashOut($s)
    {
//        $s = new Kas;
        $this->s = $s;
    }
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Doc. Ref", $this->s->doc_ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", sql2date($this->s->doc_ref, "dd-MMM-yyyy"));
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("To", $this->s->who);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("No. Receipt", $this->s->no_kwitansi);
        $raw .= $newLine;
        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
        $raw .= parent::addHeaderSales("Cashier", $user->name);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Note", $this->s->keperluan);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= "COA    Note                                          TOTAL";
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $disc = 0;
        foreach ($this->s->kasDetails as $d) {
//            $d = new KasDetail;
            $raw .= self::addItemCode($d->account_code,
                '', '',
                number_format(abs($d->total), 2));
            $raw .= $newLine;
            $raw .= parent::addItemNameReceipt($d->item_name, CHARLENGTHRECEIPT,7);
            $raw .= $newLine;
        }
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Total", number_format(abs($this->s->total), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", self::fillWithChar("-", 17));
        $raw .= $newLine;
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        $raw .= '.';
        $raw .= $newLine;
        U::save_file(ReportPath.$this->s->doc_ref.'.txt',$raw);
        return $raw;
    }
    function addItemCode($itemKode, $qty, $price, $subtotal, $l = CHARLENGTHRECEIPT)
    {
        $litemKode = strlen($itemKode); // max 15
        $mitemKode = 15;
        $lqty = strlen($qty); //max 7
        $mqty = 7;
        $lprice = strlen($qty); //max 17
        $mprice = 20;
        $lsubtotal = strlen($subtotal); //max 17
        $msubtotal = 21;
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
//            $litemKode = $mitemKode;
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mitemKode);
//            $lqty = $mqty;
        }
        if ($lprice > $mprice) {
            $price = substr($price, 0, $mprice);
//            $lqty = $mqty;
        }
        if ($lsubtotal > $msubtotal) {
            $subtotal = substr($subtotal, 0, $msubtotal);
//            $lsubtotal = $msubtotal;
        }
        $msg1 = parent::addLeftRight($itemKode, $qty, $mitemKode + $mqty);
        $msg2 = parent::addLeftRight($msg1, $price, $mitemKode + $mqty + $mprice);
        return parent::addLeftRight($msg2, $subtotal, $l);
    }
} 